#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char **argv){

	char *inicio=argv[1];
	char *destino=argv[2];
	int n, err;
	unsigned char buffer[4096];
	int fini=open(inicio,O_RDONLY);
	int fdes=open(destino,O_CREAT|O_WRONLY|O_TRUNC,S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
	if(fini<0){
		printf("No se pudo leer archivo\n");
		exit(1);
	}
	if(fdes<0){
		printf("No se pudo crear archivo\n");
		exit(1);
	}
	while(1){
		err=read(fini,buffer,4096);
		if(err==-1){
			printf("Error al leer\n");
			exit(1);
		}
		n=err;
		if(n==0){
			break;
		}
		err=write(fdes,buffer,n);
		if(err==-1){
			printf("Error al escribir\n");
			exit(1);
		}
	}
	long int size=lseek(fdes,0,SEEK_END);
	printf("%li Bytes copiados\n",size);
	close(fini);
	close(fdes);
	return 0;
}
